const express = require('express')
const socketio = require('socket.io')

var app = express()
var server = app.listen(5000)
var io = socketio(server)

app.use(express.static('public'))
app.use(express.static('node_modules'))

io.on('connection', (socket) => {
    socket.on('socketping', () => {
        console.log('Received socketping. Initiating process...')
        let done = false;
        let recordCount = 10000;
        for (let i=1; i<=recordCount; i++) {
            let percentage = Math.floor(recordCount / i) / 100
            if (percentage%10 === 0) {
                socket.emit('progress', {percentage: 100 - percentage})
            }
        }
        socket.emit('done')
    })
})
